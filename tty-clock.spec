Name:           tty-clock
Version:        2.3 
Release:        6%{?dist}
Summary:        Simple terminal clock

License:        BSD
URL:            https://github.com/xorg62/%{name}
Source0:        %{url}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc ncurses-devel

%description
A simple and cool-looking clock for the terminal.

%prep
%autosetup

%build
%set_build_flags
%make_build CC=gcc


%install
%make_install PREFIX=%{_prefix}


%files
%doc README
%{_bindir}/tty-clock
%{_mandir}/man1/tty-clock.1*

%changelog
* Sat Mar 16 2019 Dragos Iorga <idf31@protonmail.com> - 2.3-6
- Changed "tty-clock.1.*" to "tty-clock.1*" in the "files" section for safety
- Fixed wrong placement of Fedora compiler flags
- Separated changelogs by newline for readability

* Thu Feb 14 2019 Dragos Iorga <idf31@protonmail.com> - 2.3-5
- Added gcc as a build dependency
- Added default Fedora compiler flags
- Better URL Adress for Source0

* Wed Feb 13 2019 Dragos Iorga <idf31@protonmail.com> - 2.3-4
- Fixed bogus date
- Added gcc to BuildRequire

* Tue Feb 12 2019 Dragos Iorga <idf31@protonmail.com> - 2.3-3
- Made Source0 less repetitive

* Mon Feb 11 2019 Dragos Iorga <idf31@protonmail.com> - 2.3-2
- Fixed typo

* Sun Feb 10 2019 Dragos Iorga <idf31@protonmail.com> - 2.3-1
- Initial Commit
- Fixed broken URL
- Fixed bad paths
- Fixed wrong license name
